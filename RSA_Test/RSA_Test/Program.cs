﻿using System;
using System.Threading;
using System.Text;
using System.Data;
using System.IO;
using System.Collections;
using System.Security.Cryptography;
using Steam4NET;

namespace RSA_Test
{
    class Program
    {
        static RSACryptoServiceProvider rsa;
        static SteamContext context = new SteamContext();
        static String entangle;
        static String user;
        static Friend friend = null;

        static void FriendChatMsg_OnRun(FriendChatMsg_t param)
        {
            if (context.SteamUser.GetSteamID() == param.m_ulSender)
                return;

            byte[] message = new byte[1024 * 4];
            EChatEntryType type = EChatEntryType.k_EChatEntryTypeChatMsg;
            int len = context.SteamFriends.GetChatMessage(param.m_ulSender, (int)param.m_iChatID, message, message.Length, ref type);

            Friend friend = new Friend();

            friend.SteamID = param.m_ulSender;
            friend.PersonaName = context.SteamFriends.GetFriendPersonaName(friend.SteamID);

            string msg = Encoding.ASCII.GetString(message, 0, len - 1);
            if (type != EChatEntryType.k_EChatEntryTypeTyping && friend.PersonaName.ToLower().Contains(entangle))
            {
                if (msg.StartsWith("~%"))
                {
                    msg = msg.Substring(2);
                    File.WriteAllText("keys/" + friend.PersonaName.ToLower() + ".pub.xml", System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(msg)));

                    String key = "~§" + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(File.ReadAllText("keys/" + user + ".pub.xml")));
                    context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, System.Text.Encoding.UTF8.GetBytes(key), key.Length + 1);
                    Console.WriteLine("Sent handshake");
                }
                if (msg.StartsWith("~§"))
                {
                    msg = msg.Substring(2);
                    File.WriteAllText("keys/" + friend.PersonaName.ToLower() + ".pub.xml", System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(msg)));
                }
                if (msg.StartsWith("~#"))
                {
                    msg = msg.Substring(2);
                    rsa.FromXmlString(File.ReadAllText("keys/" + user + ".priv.xml"));
                    byte[] decoded = rsa.Decrypt(Convert.FromBase64String(msg), false);

                    Console.WriteLine(friend.PersonaName + ": " + System.Text.Encoding.UTF8.GetString(decoded));
                }
            }
        }

        static void NewKey()
        {
            Int32 bits;
            while (true)
            {
                Console.Write("] key bits: ");
                try
                {
                    bits = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    continue;
                }
                break;
            }

            rsa = new RSACryptoServiceProvider(bits);
            File.WriteAllText("keys/" + user + ".pub.xml", rsa.ToXmlString(false));
            File.WriteAllText("keys/" + user + ".priv.xml", rsa.ToXmlString(true));
        }

        static void Main(string[] args)
        {
            if (!context.Initialize())
                return;

            user = context.SteamFriends.GetPersonaName().ToLower();
            Directory.CreateDirectory("keys");
            Console.WriteLine("] initilizing RSA");
            if (!File.Exists("keys/" + user + ".pub.xml") || !File.Exists("keys/" + user + ".priv.xml"))
                NewKey();
            else
                rsa = new RSACryptoServiceProvider();

            Console.Write("] entangle with: ");
            entangle = Console.ReadLine();

            for (int i = 0; i < context.SteamFriends.GetFriendCount((int)EFriendFlags.k_EFriendFlagImmediate); ++i)
            {
                Friend _friend = new Friend();

                CSteamID steamId = context.SteamFriends.GetFriendByIndex(i, (int)EFriendFlags.k_EFriendFlagImmediate);
                string friendName = context.SteamFriends.GetFriendPersonaName(steamId);

                _friend.SteamID = steamId;
                _friend.PersonaName = friendName;

                if (_friend.PersonaName.ToLower().Contains(entangle))
                {
                    friend = _friend;
                    break;
                }
            }

            if (friend == null)
            {
                Console.WriteLine("] could not find person to entangle with!");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("] starting steam hooking");
            context.FriendChatMsg.OnRun += new Callback<FriendChatMsg_t>.DispatchDelegate(FriendChatMsg_OnRun);
            context.StartCallbacks();
            Console.WriteLine("] ready!");
            while (true)
            {
                String line = Console.ReadLine();

                if (line == "-handshake")
                {
                    String key = "~%" + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(File.ReadAllText("keys/" + user + ".pub.xml")));
                    context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, System.Text.Encoding.UTF8.GetBytes(key), key.Length + 1);
                    Console.WriteLine("Sent handshake");
                }
                else if (line == "-entangle")
                {
                    Console.Write("] entangle with: ");
                    entangle = Console.ReadLine();
                    bool found = false;

                    for (int i = 0; i < context.SteamFriends.GetFriendCount((int)EFriendFlags.k_EFriendFlagImmediate); ++i)
                    {
                        Friend _friend = new Friend();

                        CSteamID steamId = context.SteamFriends.GetFriendByIndex(i, (int)EFriendFlags.k_EFriendFlagImmediate);
                        string friendName = context.SteamFriends.GetFriendPersonaName(steamId);

                        _friend.SteamID = steamId;
                        _friend.PersonaName = friendName;

                        if (_friend.PersonaName.ToLower().Contains(entangle))
                        {
                            friend = _friend;
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        Console.WriteLine("] could not find person to entangle with!");
                    }
                }
                else
                {
                    if (!File.Exists("keys/" + friend.PersonaName.ToLower() + ".pub.xml"))
                    {
                        Console.WriteLine("] missing RSA key, please handshake!");
                        continue;
                    }
                    rsa.FromXmlString(File.ReadAllText("keys/" + friend.PersonaName.ToLower() + ".pub.xml"));
                    String data = Convert.ToBase64String(rsa.Encrypt(System.Text.Encoding.UTF8.GetBytes(line), false));
                    String text = "~#" + data;
                    context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, System.Text.Encoding.UTF8.GetBytes(text), text.Length + 1);
                }
            }
        }
    }
}
