﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using Steam4NET;

namespace anemos
{
    public class Anim
    {
        static void alias(string gif)
        {
            anemos.Program.context.SteamFriends.SetPersonaName("Olivia #" + gif);
        }
        static void alias2(string gif)
        {
            anemos.Program.context.SteamFriends.SetPersonaName("Olivia " + gif);
        }
        public static void Idle()
        {
            alias2("|");
            Thread.Sleep(500);
            alias2("/");
            Thread.Sleep(500);
            alias2("—");
            Thread.Sleep(500);
            alias2("\\");
            Thread.Sleep(500);
            alias2("|");
            Thread.Sleep(500);
            alias2("/");
            Thread.Sleep(500);
            alias2("—");
            Thread.Sleep(500);
            alias2("\\");
            Thread.Sleep(500);
            Idle();
        }
        public static void Initialize()
        {
            alias("");
            Thread.Sleep(500);
            alias("i");
            Thread.Sleep(500);
            alias("in");
            Thread.Sleep(500);
            alias("ini");
            Thread.Sleep(500);
            alias("init");
            Thread.Sleep(2500);
            alias("ini-");
            Thread.Sleep(500);
            alias("in-");
            Thread.Sleep(500);
            alias("i-");
            Thread.Sleep(500);
            alias("");
            Thread.Sleep(500);
        }
        public static void Chat()
        {
            alias("");
            Thread.Sleep(500);
            alias("j");
            Thread.Sleep(500);
            alias("jo");
            Thread.Sleep(500);
            alias("joi");
            Thread.Sleep(500);
            alias("join");
            Thread.Sleep(2500);
            alias("joi-");
            Thread.Sleep(500);
            alias("jo-");
            Thread.Sleep(500);
            alias("j-");
            Thread.Sleep(500);
            alias("");
            Thread.Sleep(500);
        }
    }
}
