using System;
using System.Threading;
using System.Text;
using System.Data;
using System.IO;
using System.Collections;
using System.Security.Cryptography;
using Steam4NET;

namespace anemos
{
    class Program
    {
        public static SteamContext context = new SteamContext();
        static void ChatRoomEnter_OnRun( ChatRoomEnter_t param )
        {
            Anim.Chat();
        }
        static void FriendChatMsg_OnRun(FriendChatMsg_t param)
        {
            if (context.SteamUser.GetSteamID() == param.m_ulSender)
                return;

            byte[] message = new byte[1024 * 4];
            EChatEntryType type = EChatEntryType.k_EChatEntryTypeChatMsg;
            int len = context.SteamFriends.GetChatMessage(param.m_ulSender, (int)param.m_iChatID, message, message.Length, ref type);

            Friend friend = new Friend();

            friend.SteamID = param.m_ulSender;
            friend.PersonaName = context.SteamFriends.GetFriendPersonaName(friend.SteamID);

            string msg = Encoding.ASCII.GetString(message, 0, len - 1);
            if (type != EChatEntryType.k_EChatEntryTypeTyping)
            {
            }
        }
        static void Main(string[] args)
        {
            Console.Write("] mara's anemos initializing...");
            if (!context.Initialize())
                return;

            context.SteamFriends.SetPersonaName("Mara #");

            string user = context.SteamFriends.GetPersonaName().ToLower();
            Console.Write(" with user " + user + "...\n");
            Console.WriteLine("] anemos creating hooks...");
            Console.WriteLine("\tFriendChatMsg_OnRun");
            context.ChatRoomEnter.OnRun += new Callback<ChatRoomEnter_t>.DispatchDelegate(ChatRoomEnter_OnRun);
            //context.FriendChatMsg += new Callback<FriendChatMsg_t>.DispatchDelegate(FriendChatMsg_OnRun);


            Console.WriteLine("] anemos injecting hooks...");
            context.StartCallbacks();
            
            Console.WriteLine("] anemos creating buffer...");
            while (true)
            {
                Anim.Initialize();
                string input = Console.ReadLine();

                if (input == "idle")
                {
                    Anim.Idle();
                    Console.ReadLine();
                }
                //testloop();
            }
        }
    }
}
