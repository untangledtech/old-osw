#ifndef _STEAMKIT_TCPSOCKET
#define _STEAMKIT_TCPSOCKET

#include<Windows.h>

class TcpSocket
{
public:
	TcpSocket();
	~TcpSocket();

	bool SetBlocking(bool blocking);
	bool Connect(char *address);
	bool Disconnect();
	bool Send(void *data, unsigned long len);
	bool Receive(void *data, unsigned long len);

	int GetStatus();
	int ResetStatus();
};

#endif