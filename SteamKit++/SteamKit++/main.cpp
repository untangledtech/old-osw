#include<Windows.h>

int main()
{
	WSADATA WSAData;
	if (WSAStartup(MAKEWORD(2,0), &WSAData))
		return 1;

	SOCKET sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.S_un.S_addr = inet_addr("72.165.61.189");
	addr.sin_port = htons(27030);
	if (connect(sock, (sockaddr*)&addr, sizeof(sockaddr_in)))
		return 1;
	char buff1[4] = {2, 0, 0, 0};
	if (!send(sock, buff1, 4, 0))
		return 1;
	char res1[1];
	if (!recv(sock, res1, 1, 0))
		return 1;
	char buff2[5] = {0, 0, 0, 1, 3};
	if (!send(sock, buff2, 5, 0))
		return 1;
	char res2[6];
	if (!recv(sock, (char*)&res2, 6, 0))
		return 1;

	closesocket(sock);

	WSACleanup();
}