#ifndef _STEAMKIT_STEAMCONNECTION
#define _STEAMKIT_STEAMCONNECTION

#include"udpsocket.h"
#include"tcpsocket.h"

class SteamConnection
{
public:
	SteamConnection();
	~SteamConnection();

	void Connect();
	void Disconnect();
	void Poll();

	void SetLoginDetails(char *username, char *password);
private:
	char *username;
	char *password;

	bool tcp;
};

#endif