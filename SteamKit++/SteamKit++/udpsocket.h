#ifndef _STEAMKIT_UDPSOCKET
#define _STEAMKIT_UDPSOCKET

#include<Windows.h>

class UdpSocket
{
public:
	UdpSocket();
	~UdpSocket();

	bool SetBlocking(bool blocking);
	bool Send(char *address, void *data, unsigned long len);
	bool Receive(char *address, void *data, unsigned long len);
}

#endif