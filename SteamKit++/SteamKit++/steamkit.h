#define BYTESWAP_SHORT(x) _byteswap_ushort(x)
#define BYTESWAP_LONG(x) _byteswap_ulong(x)

#include<Windows.h>
#include"steamconnection.h"

enum SteamKit_Error
{
	ERROR_NONE = 0,
	ERROR_ALREADY, // Function has already been ran, and should only be ran once
	ERROR_NOTSTATUS, // Function cannot be executed under current status code (something else should probably be called first)
	ERROR_NETINIT, // Network init failure
};

enum SteamKit_Status
{
	STATUS_INITIAL = 0,
	STATUS_STARTED,
	STATUS_LOGGINGIN,
	STATUS_LOGINFAILED,
	STATUS_LOGGEDIN,
};

enum SteamKit_Flags
{
	FLAG_BLOCK = 1 << 0,
};

class SteamKit
{
public:
	SteamKit();
	~SteamKit();

	virtual bool Startup(int flags = 0);
	virtual bool Login(char *username, char *password, int flags = 0);
	virtual void Poll();

	virtual SteamKit_Status GetStatus() { return status; }

	virtual SteamKit_Error GetError() { return error; }
protected:
	virtual void SetError(SteamKit_Error err) { error = err; }
	virtual void SetStatus(SteamKit_Status stat) { status = stat; }

	SteamKit_Error error;
	SteamKit_Status status;

	SteamConnection *steamConnection;
};

#ifndef STEAMKIT_DLL
	extern "C" __declspec(dllimport) SteamKit *CreateSteamKit();
#endif