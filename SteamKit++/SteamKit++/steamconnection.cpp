#include"steamconnection.h"

SteamConnection::SteamConnection()
{
	tcp = false;
	username = NULL;
	password = NULL;
}

SteamConnection::~SteamConnection()
{
	if (username)
		delete username;
	if (password)
		delete password;
}

void SteamConnection::SetLoginDetails(char *user, char *pass)
{
	// Details are copied to separate variables where they are deleted when finished connecting
	if (username)
		delete username;
	if (password)
		delete password;

	username = new char[strlen(user) + 1];
	password = new char[strlen(pass) + 1];
	memcpy(username, user, strlen(user) + 1);
	memcpy(password, user, strlen(pass) + 1);
}

void SteamConnection::Connect()
{
}

void SteamConnection::Disconnect()
{
}

void SteamConnection::Poll()
{
}