#include"steamkit.h"

extern "C" __declspec(dllexport) SteamKit *CreateSteamKit()
{
	return new SteamKit();
}

SteamKit::SteamKit()
{
	steamConnection = NULL;
	SetError(ERROR_NONE);
	SetStatus(STATUS_INITIAL);
}

SteamKit::~SteamKit()
{
}

bool SteamKit::Startup(int flags)
{
	if (GetStatus() >= STATUS_STARTED)
	{
		SetError(ERROR_ALREADY);
		return false;
	}

	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2,0), &wsaData))
	{
		SetError(ERROR_NETINIT);
		return false;
	}

	SetStatus(STATUS_STARTED);

	return true;
}

bool SteamKit::Login(char *username, char *password, int flags)
{
	if (GetStatus() >= STATUS_LOGGEDIN || GetStatus() == STATUS_LOGGINGIN)
	{
		SetError(ERROR_ALREADY);
		return false;
	}
	else if (GetStatus() < STATUS_STARTED)
	{
		SetError(ERROR_NOTSTATUS);
		return false;
	}

	SetStatus(STATUS_LOGGINGIN);
	steamConnection = new SteamConnection();
	steamConnection->SetLoginDetails(username, password);
	steamConnection->Connect();

	if (flags & FLAG_BLOCK)
	{
		while (GetStatus() < STATUS_LOGINFAILED)
		{
			Poll();
			Sleep(1);
		}
	}

	return true;
}

void SteamKit::Poll()
{
	if (steamConnection)
		steamConnection->Poll();
}