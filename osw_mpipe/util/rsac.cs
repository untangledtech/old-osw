﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Text;

namespace mpipe
{
    class rsac
    {
        private static RSACryptoServiceProvider ccrypto;
        public static void newkey()
        {
            Int32 bits;
            while (true)
            {
                log.p("key bits: ", false);
                try
                {
                    bits = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    continue;
                }
                break;
            }
            if (!(bits < 384))
            {
                ccrypto = new RSACryptoServiceProvider(bits);

                log.p("writing your " + bits.ToString( ) + " bit key... ", false);
                File.WriteAllText("keys/" + Program.user.Replace(" #", "") + ".pub.xml", ccrypto.ToXmlString(false));
                File.WriteAllText("keys/" + Program.user.Replace(" #", "") + ".priv.xml", ccrypto.ToXmlString(true));
                Console.WriteLine("done!");
            }
            else
            {
                log.p("invalid key size!");
            }
        }
    }
}
