# RubySteam
# A Steam Mobile API
# HTTPS packets found by WAYWO
require "net/http";
require "net/https";
require "uri";

module Steam
	AUTH_PUBLIC = "DE45CD6";
	AUTH_BETA = "7DC60112";
	AUTH_DEV = "E77327FA";
	
	def session( user, passwd, sguard, auth = AUTH_PUBLIC )
		@scope = "read_profile,write_profile,read_client,write_client";
			@scope = @scope.sub( ",", "%20" );
			
		steam_url = URI.parse( "https://api.steampowered.com/ISteamOAuth2/GetTokenWithCredentials/v0001/" );
		postdata = "client_id=#{hash}&grant_type=password&username=#{user}&password=#{passwd}&scope=@#{scope}";
		headers = {
			"Method" => "POST",
			"Content-Type" => "application/x-www-form-urlencoded",
			"User-Agent" => "Steam App / Android / 1.0 / 1297579",
			"Keep-Alive" => true
		}
		
		http = Net::HTTP.new( url.host, url.port );
		http.use_ssl = true;
		
		return http.post( steam_url.path, postdata, headers );
	end;
end;

session("oliviaviridis", 