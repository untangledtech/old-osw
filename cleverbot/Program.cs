﻿using System;
using System.Threading;
using System.Text;
using System.Data;
using System.IO;
using System.Collections;
using System.Security.Cryptography;
using Steam4NET;
using ChatterBotAPI;

namespace cleverbot
{
    class Program
    {
        static SteamContext context = new SteamContext();
        static ChatterBotFactory factory = new ChatterBotFactory();

        static ChatterBot bot = factory.Create(ChatterBotType.CLEVERBOT);
        static ChatterBotSession botsession = bot.CreateSession();

        static void FriendChatMsg_OnRun(FriendChatMsg_t param)
        {
            if (context.SteamUser.GetSteamID() == param.m_ulSender)
                return;

            byte[] message = new byte[1024 * 4];
            EChatEntryType type = EChatEntryType.k_EChatEntryTypeChatMsg;
            int len = context.SteamFriends.GetChatMessage(param.m_ulSender, (int)param.m_iChatID, message, message.Length, ref type);

            Friend friend = new Friend();

            friend.SteamID = param.m_ulSender;
            friend.PersonaName = context.SteamFriends.GetFriendPersonaName(friend.SteamID);

            string msg = Encoding.ASCII.GetString(message, 0, len - 1);
            if (type != EChatEntryType.k_EChatEntryTypeTyping)
            {
                if (msg.Contains("!!"))
                {
                    msg = msg.Replace("!!", "");
                    Console.WriteLine("] " + msg);
                    context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes("Thinking about that..."), Encoding.ASCII.GetBytes("Thinking about that...").Length + 1);
                        string response = botsession.Think(msg);
                        Console.WriteLine("]] " + response);
                        context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes(response), Encoding.ASCII.GetBytes(response).Length + 1);
                }
            }
        }
        static void Main(string[] args)
        {
            Console.Write("] cleverbot initializing");
            if (!context.Initialize())
                return;

            string user = context.SteamFriends.GetPersonaName().ToLower();
            Console.Write(" with user " + user + "...\n");

            Console.WriteLine("] cleverbot creating hooks...");
            context.FriendChatMsg.OnRun += new Callback<FriendChatMsg_t>.DispatchDelegate(FriendChatMsg_OnRun);

            Console.WriteLine("] cleverbot injecting hooks...");
            context.StartCallbacks();

            for (int i = 0; i < context.SteamFriends.GetFriendCount((int)EFriendFlags.k_EFriendFlagImmediate); ++i)
            {
                Friend friend = new Friend();

                CSteamID steamId = context.SteamFriends.GetFriendByIndex(i, (int)EFriendFlags.k_EFriendFlagImmediate);
                string friendName = context.SteamFriends.GetFriendPersonaName(steamId);

                friend.SteamID = steamId;
                friend.PersonaName = friendName;

                if (friend.PersonaName.ToLower().Contains("turtles"))
                {
                    context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes("Cleverbot loaded, send messages using !!MESSAGE."), Encoding.ASCII.GetBytes("Cleverbot loaded, send messages using !!MESSAGE").Length + 1);
                }
            }
            while (true)
            {
                string inputs = Console.ReadLine();
            }
        }
    }
}
