﻿using System;
using System.Threading;
using System.Text;
using System.Data;
using System.IO;
using System.Collections;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using Steam4NET;

namespace auradle
{
    /*
    [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
    delegate Int32 NativeGetChatRoomEntry(IntPtr thisobj, UInt64 steamIDchat, Int32 iChatID, ref UInt64 steamIDuser, byte[] pvData, Int32 cubData, ref EChatEntryType peChatEntryType);

    [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
    delegate string NativeGetChatRoomName(IntPtr thisobj, UInt64 steamIDchat);
    */

    class Program
    {
        /*
        public static NativeGetChatRoomEntry get_cmsg;
        public static NativeGetChatRoomName get_cname;
        */
        public static string hour = DateTime.Now.ToString("HH");
        public static string h2 = DateTime.Now.ToString("dd");
        public static string day = DateTime.Now.ToString("MM/dd/yyyy ") + DateTime.Now.ToString("t");

        public static SteamContext context = new SteamContext();

        public static String user;
        public static Friend friend = null;

        public static int Clamp(int value, int min, int max)
        {
            if (value < min)
                return min;

            if (value > max)
                return max;

            return value;
        }
        static void ChatRoomEnter_OnRun(ChatRoomEnter_t param)
        {
            //anim.chat();
        }
        static void OpenChatDialog_OnRun(OpenChatDialog_t param)
        {
            //anim.chat();
        } 
        static void FriendChatMsg_OnRun(FriendChatMsg_t param)
        {
            if (context.SteamUser.GetSteamID() == param.m_ulSender)
                return;
            byte[] message = new byte[1024 * 4];
            EChatEntryType type = EChatEntryType.k_EChatEntryTypeChatMsg;
            int len = context.SteamFriends.GetChatMessage(param.m_ulSender, (int)param.m_iChatID, message, message.Length, ref type);

            Friend friend = new Friend();

            friend.SteamID = param.m_ulSender;
            friend.PersonaName = context.SteamFriends.GetFriendPersonaName(friend.SteamID);
            if (context.SteamUser.GetSteamID() == param.m_ulSender) {
                log.p(Encoding.ASCII.GetString(message, 0, len));
                return;
            }
            string msg = Encoding.ASCII.GetString(message, 0, len - 1);
            bool authed = false;
            if (type != EChatEntryType.k_EChatEntryTypeTyping && type != EChatEntryType.k_EChatEntryTypeLeftConversation)
            {
                if (msg.Contains("#"))
                {
                    foreach (string autheduser in data.auth)
                    {
                        if (friend.SteamID.ToString() == autheduser)
                        {
                            authed = true;
                        }
                    }
                    if (authed)
                    {
                        msg = msg.Replace("#", "");
                        log.m(msg, friend.PersonaName + " - " + friend.SteamID);

                        if (msg.ToLower() == "kill idlers" || msg.ToLower() == "stop idlers")
                        {
                            idler.kill();
                            //anim.idle();
                        }
                        else if (msg.ToLower().Contains("exec "))
                        {
                            msg = msg.Replace("exec ", "");
                            log.p("executing " + msg + ":");
                            idler.exec(msg, true);
                        }
                        else if (msg.ToLower() == "create idlers" || msg.ToLower() == "start idlers")
                        {
                            log.p("auradle creating idlers...");
                            int count = 0;
                            foreach (string client in data.idlers)
                            {
                                count = count + 1;
                                idler.create(count, client);
                            }
                            //anim.idle();
                        }
                    }
                    else
                    {
                        log.p("unauthorized user " + friend.PersonaName + " has attempted to execute" + msg + "!");
                        string mmsg = "You are not an authorized user, this instance has been reported ~" + friend.SteamID.ToString();
                        context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes(mmsg), Encoding.ASCII.GetBytes(mmsg).Length + 1);
                    }
                }
            }
        }
        static void PersonaStateChange_OnRun( PersonaStateChange_t packet )
        {
            if (context.SteamFriends.GetFriendRelationship(packet.m_ulSteamID) == EFriendRelationship.k_EFriendRelationshipFriend)
            {
                string frnd = context.SteamFriends.GetFriendPersonaName(packet.m_ulSteamID);
                log.p(frnd + " has changed their state to " + context.SteamFriends.GetFriendPersonaState(packet.m_ulSteamID));
            }
        }
        /*
        static void ChatRoomMsg_OnRun(ChatRoomMsg_t msg)
        {
            byte[] mdata = new byte[1024 * 4];
            EChatEntryType type = EChatEntryType.k_EChatEntryTypeInvalid;
            ulong chatter = 0;

            string cname = get_cname(context.SteamFriends.Interface, msg.m_ulSteamIDChat);
            int len = get_cmsg(context.SteamFriends.Interface, msg.m_ulSteamIDChat, (int)msg.m_iChatID, ref chatter, mdata, mdata.Length, ref type);

            len = Clamp(len, 1, mdata.Length);

            log.gm(Encoding.UTF8.GetString(mdata, 0, len), get_cname(context.SteamFriends.Interface, msg.m_ulSteamIDChat), context.SteamFriends.GetFriendPersonaName(new CSteamID(msg.m_ulSteamIDUser)));
        }
        */
        static void Main(string[] args)
        {
            log.p("auradle initializing", false);
            if (!context.Initialize())
                return;

            Program.user = context.SteamFriends.GetPersonaName();

            Console.WriteLine(" with user " + user + "...");

            log.p("auradle creating hooks...");

            Console.WriteLine("\t\tFriendChatMsg_OnRun");
            context.FriendChatMsg.OnRun += new Callback<FriendChatMsg_t>.DispatchDelegate(FriendChatMsg_OnRun);

            Console.WriteLine("\t\tPersonaStateChange_OnRun");
            context.PersonaStateChange.OnRun += new Callback<PersonaStateChange_t>.DispatchDelegate(PersonaStateChange_OnRun);

            //Console.WriteLine("\t\t\tChatRoomMsg_OnRun");
            //context.ChatRoomMsg.OnRun += new Callback<ChatRoomMsg_t>.DispatchDelegate(ChatRoomMsg_OnRun);

            //Console.WriteLine("\t\t\tChatRoomEnter_OnRun");
            //context.ChatRoomEnter.OnRun += new Callback<ChatRoomEnter_t>.DispatchDelegate(ChatRoomEnter_OnRun);

            //Console.WriteLine("\t\t\tOpenChatDialog_OnRun");
            //context.OpenChatDialog.OnRun += new Callback<OpenChatDialog_t>.DispatchDelegate(OpenChatDialog_OnRun);

            log.p("auradle injecting hooks...");
            context.StartCallbacks();

            log.p("auradle creating buffer...");
            if (!Directory.Exists("logs"))
            {
                Directory.CreateDirectory("logs");
            }

            log.p("auradle creating idlers...");
            int count = 0;
            foreach (string client in data.idlers)
            {
                count = count + 1;
                idler.create(count, client);
            }
            while (true)
            {
                Console.ReadLine();
                Thread.Sleep(0);
            }
        }
    }
}
