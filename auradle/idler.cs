﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace auradle
{
    class idler
    {
        public static bool server_exists = false;
        public static void exec(object command, bool routput = false)
        {
            try
            {
                System.Diagnostics.ProcessStartInfo procStartInfo =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);

                procStartInfo.RedirectStandardOutput = routput;
                procStartInfo.UseShellExecute = false;

                procStartInfo.CreateNoWindow = true;

                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();

                if (routput)
                {
                    string result = proc.StandardOutput.ReadToEnd();

                    log.p("\t" + result);
                }
            }
            catch (Exception objException)
            {
                log.p("error! " + objException.ToString());
            }
        }
        public static void create(int num, string client)
        {

            Console.Write("\t\t" + num.ToString() + ": creating ");

            Console.BackgroundColor = ConsoleColor.Black;
            ConsoleColor color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), "DarkYellow");
            Console.ForegroundColor = color;

            Console.Write(client);
            Console.ResetColor();

            Console.Write("... ");

            if (!server_exists)
            {
                exec("start /low C:\\\"Program Files\"\\Sandboxie\\Start.exe /box:" + client + " Steam.exe -applaunch 440 -textmode -novid -nosound -noipx -nopreload +map \"itemtest\" +exec idle");
            }
            else
            {
                exec("start /low C:\\\"Program Files\"\\Sandboxie\\Start.exe /box:" + client + " Steam.exe -applaunch 440 -textmode -novid -nosound -noipx -nopreload +join 127.0.0.1 +exec idle");
                server_exists = true;
            }
            Console.Write("success!\n");
        }
        private static int count = 0;
        public static void kill()
        {
            //log.p("received pkill!");
            Process[] processes = Process.GetProcessesByName("hl2.exe");

            foreach (Process process in processes)
            {
                count = count + 1;
                log.p(count.ToString( ) + ": shutting down hl2.exe... ", false);
                process.Kill();
                Console.WriteLine("success!");
            }
            count = 0;
            //log.p("done!");
        }
    }
}
