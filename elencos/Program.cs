﻿using System;
using System.Threading;
using System.Text;
using System.Data;
using System.IO;
using System.Collections;
using System.Security.Cryptography;
using Steam4NET;

namespace elencos
{
    class Program
    {
        static SteamContext context = new SteamContext();
        static void FriendChatMsg_OnRun(FriendChatMsg_t param)
        {
            if (context.SteamUser.GetSteamID() == param.m_ulSender)
                return;

            byte[] message = new byte[1024 * 4];
            EChatEntryType type = EChatEntryType.k_EChatEntryTypeChatMsg;
            int len = context.SteamFriends.GetChatMessage(param.m_ulSender, (int)param.m_iChatID, message, message.Length, ref type);

            Friend friend = new Friend();

            friend.SteamID = param.m_ulSender;
            friend.PersonaName = context.SteamFriends.GetFriendPersonaName(friend.SteamID);

            string msg = Encoding.ASCII.GetString(message, 0, len - 1);
            if (type != EChatEntryType.k_EChatEntryTypeTyping)
            {
                string filename = "";
                if (msg.Contains("#"))
                {
                    msg = msg.Replace("#", "");
                    Console.WriteLine("# " + friend.PersonaName.ToLower() + " " + msg);

                    if (msg.Contains("login"))
                    {
                        Console.Write("\tscanning authenticated usertable for user " + friend.PersonaName.ToLower() + "... ");
                        if (!Directory.Exists("data"))
                        {
                            Console.Write("\t\tdata folder not found, creating... ");
                            Directory.CreateDirectory("data");

                            Console.WriteLine("success!");
                        }
                        if (!File.Exists("data/usertable.elen"))
                        {
                            Console.Write("\t\tusertable not found, creating... ");
                            File.WriteAllText("data/usertable.elen", "");

                            Console.WriteLine("success!");
                        }
                        if (File.ReadAllText("data/usertable.elen").Contains(friend.SteamID.AccountID.ToString()))
                        {
                            context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes("User " + friend.PersonaName.ToLower() + " #" + friend.SteamID.AccountID.ToString() + " sucessfully authenticated"), Encoding.ASCII.GetBytes("User " + friend.PersonaName.ToLower() + " #" + friend.SteamID.AccountID.ToString() + " sucessfully authenticated").Length + 1);
                        }
                        else
                        {
                            context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes("I'm sorry, " + friend.PersonaName.ToLower() + " #" + friend.SteamID.AccountID.ToString() + ", you don't seem to be in the authenticated usertable."), Encoding.ASCII.GetBytes("I'm sorry, " + friend.PersonaName.ToLower() + " #" + friend.SteamID.AccountID.ToString() + ", you don't seem to be in the authenticated usertable.").Length + 1);
                            //context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes("I'm sorry, " + friend.PersonaName.ToLower() + " #" + friend.SteamID.AccountID.ToString() + ", you don't seem to be in the authenticated usertable."), Encoding.ASCII.GetBytes("I'm sorry, " + friend.PersonaName.ToLower() + " #" + friend.SteamID.AccountID.ToString() + ", you don't seem to be in the authenticated usertable.").Length + 1);
                        }
                    }
                }
                if (msg.Contains("~*"))
                {
                    Console.WriteLine("]accepted file name " + msg);
                    msg = msg.Replace("~*", "");
                    Console.WriteLine("]accepted file name " + msg);
                    msg = Convert.ToString(msg);
                    Console.WriteLine("]accepted file name " + msg);
                    filename = msg;
                }
                if (msg.Contains("~^"))
                {
                    msg = msg.Replace("~^", "");
                    Console.WriteLine("] would you like to accept" + filename + " from user " + friend.PersonaName.ToLower() + "? y/n");
                    Console.WriteLine("] file size is " + (Convert.ToInt32(msg.Length) / 8).ToString() + " bytes");

                    string res = Console.ReadLine();
                    res = res.ToLower();

                    if (res == "y" || res == "yes")
                    {
                        if (!Directory.Exists("data"))
                        {
                            Directory.CreateDirectory("data");
                        }
                        if (!Directory.Exists("data/" + friend.PersonaName.ToLower()))
                        {
                            Directory.CreateDirectory("data/" + friend.PersonaName.ToLower());
                        }

                        Console.WriteLine("] writing " + filename + " to data/" + friend.PersonaName.ToLower() + "/" + filename + "...");
                        File.WriteAllBytes("data/" + friend.PersonaName.ToLower() + "/" + filename, Convert.FromBase64String(msg));
                    }
                    else
                    {
                        Console.WriteLine("] file transfer from user" + friend.PersonaName.ToLower() + " denied...");
                    }
                }
                if (msg.Contains("~#"))
                {
                    msg = msg.Replace("~#", "");
                    string key = "_ERROR_";
                    if (Directory.Exists("keytable"))
                    {
                        if (File.Exists("keytable/" + friend.PersonaName.ToLower( ) + ".key"))
                        {
                            key = File.ReadAllText("keytable/" + friend.PersonaName.ToLower( ) + ".key");
                        }
                        else
                        {
                            Console.WriteLine("] key doesn't exist for user " + friend.PersonaName.ToLower() + "!");
                        }
                    }
                    else
                    {
                        Console.WriteLine("] key doesn't exist for user " + friend.PersonaName.ToLower() + "!");
                    }
                    if (key != "_ERROR")
                    {
                        RC4 crypto = new RC4(key);
                        string cipher = crypto.DecryptString(Convert.FromBase64String(msg));

                        Console.WriteLine(friend.PersonaName.ToLower( ) + ": " + cipher);
                    }
                }
            }
        }
        static string rstring(int length)
        {
            Random random = new Random();
            String randomString = "";
            int randNumber;

            for (int i = 0; i < length; i++)
            {
                if (random.Next(1, 3) == 1)
                    randNumber = random.Next(97, 126); //char {a-z}
                else
                    randNumber = random.Next(33, 58); //int {0-9}

                randomString = randomString + (char)randNumber;
            }
            return randomString;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("] elencos using rc4 cryptosystem...");
            Console.Write("] elencos initializing");
            if (!context.Initialize())
                return;

            string user = context.SteamFriends.GetPersonaName().ToLower();
            Console.Write(" with user " + user + "...\n");
            Console.WriteLine("] elencos loading keytable...");

            string key;
            if (Directory.Exists("keytable"))
            {
                if (File.Exists("keytable/" + user + ".key"))
                {
                    string[] keytable = Directory.GetFiles("keytable", "*.key");
                    foreach( string keyy in keytable )
                    {
                        string communicator = keyy.Replace( "keytable\\", "").Replace(".key", "");
                        Console.WriteLine( "\t" + communicator + " " + File.ReadAllText("keytable/" + communicator + ".key" ).Length );
                    }
                    key = File.ReadAllText("keytable/" + user + ".key");
                }
                else
                {
                    Console.WriteLine("] how large do you want your key to be (bits)?");
                    string size = Console.ReadLine();

                    Console.WriteLine("] generating key (" + size + ")...");
                    string tkey = rstring(Convert.ToInt32(size));

                    Console.WriteLine(tkey);

                    Console.WriteLine("writing key to keytable...\n");
                    File.WriteAllText("keytable/" + user + ".key", tkey);

                    key = File.ReadAllText("keytable/" + user + ".key");
                }
            }
            else
            {
                Console.WriteLine("] invalid keytable...");
                Directory.CreateDirectory("keytable");

                Console.WriteLine("] how large do you want your key to be (bits)?");
                string size = Console.ReadLine();

                Console.WriteLine("] generating key (" + size + ")...");
                string tkey = rstring(Convert.ToInt32(size));

                Console.WriteLine(tkey);

                Console.WriteLine("] writing key to keytable...\n");
                File.WriteAllText("keytable/" + user + ".key", tkey);

                key = File.ReadAllText("keytable/" + user + ".key");
            }

            Console.WriteLine("] who would you like to entangle with?");
            string entangle = Console.ReadLine();

            Console.WriteLine("] elencos creating hooks...");
            context.FriendChatMsg.OnRun += new Callback<FriendChatMsg_t>.DispatchDelegate(FriendChatMsg_OnRun);

            Console.WriteLine("] elencos injecting hooks...");
            context.StartCallbacks();

            while (true)
            {
                string inputs = Console.ReadLine();
                if (inputs == "handshake")
                {
                    for (int i = 0; i < context.SteamFriends.GetFriendCount((int)EFriendFlags.k_EFriendFlagImmediate); ++i)
                    {
                        Friend friend = new Friend();

                        CSteamID steamId = context.SteamFriends.GetFriendByIndex(i, (int)EFriendFlags.k_EFriendFlagImmediate);
                        string friendName = context.SteamFriends.GetFriendPersonaName(steamId);

                        friend.SteamID = steamId;
                        friend.PersonaName = friendName;

                        if (friend.PersonaName.ToLower().Contains(entangle))
                        {
                            context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes("Beginning elencos handshake..."), Encoding.ASCII.GetBytes("Beginning elencos handshake...").Length + 1);
                            context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes("~%" + key), Encoding.ASCII.GetBytes("~%" + key).Length + 1);
                        }
                    }
                }
                if (inputs == "push")
                {
                    Console.WriteLine("] what file do you want me to push to " + entangle + "?");
                    string fp = Console.ReadLine();

                    for (int i = 0; i < context.SteamFriends.GetFriendCount((int)EFriendFlags.k_EFriendFlagImmediate); ++i)
                    {
                        Friend friend = new Friend();

                        CSteamID steamId = context.SteamFriends.GetFriendByIndex(i, (int)EFriendFlags.k_EFriendFlagImmediate);
                        string friendName = context.SteamFriends.GetFriendPersonaName(steamId);

                        friend.SteamID = steamId;
                        friend.PersonaName = friendName;

                        if (friend.PersonaName.ToLower().Contains(entangle))
                        {
                            context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes("~*" + fp ), Encoding.ASCII.GetBytes("~*" + fp).Length + 1);
                            context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes("~^" + Convert.ToBase64String(File.ReadAllBytes(fp))), Encoding.ASCII.GetBytes("~^" + Convert.ToBase64String(File.ReadAllBytes(fp))).Length + 1);
                        }
                    }
                }
                UnicodeEncoding bytec = new UnicodeEncoding();
                byte[] input = bytec.GetBytes(inputs);

                //Console.WriteLine("] encrypting with " + user + "'s key...");
                RC4 crypto = new RC4(key);
                string cipher = "~#" + Convert.ToBase64String(crypto.Encrypt(input));
                Console.WriteLine(user + ": " + inputs);

                for (int i = 0; i < context.SteamFriends.GetFriendCount((int)EFriendFlags.k_EFriendFlagImmediate); ++i)
                {
                    Friend friend = new Friend();

                    CSteamID steamId = context.SteamFriends.GetFriendByIndex(i, (int)EFriendFlags.k_EFriendFlagImmediate);
                    string friendName = context.SteamFriends.GetFriendPersonaName(steamId);

                    friend.SteamID = steamId;
                    friend.PersonaName = friendName;

                    if (friend.PersonaName.ToLower().Contains(entangle))
                    {
                        context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, Encoding.ASCII.GetBytes(cipher), Encoding.ASCII.GetBytes(cipher).Length + 1);
                    }
                }
            }
        }
    }
}
