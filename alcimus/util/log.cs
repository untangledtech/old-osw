﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace alcimus
{
    public class log
    {
        public static void p(string msg, bool nl = true)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            ConsoleColor color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), "DarkYellow");
            Console.ForegroundColor = color;

            Console.Write("[" + DateTime.Now.ToString("T") + "] ");

            Console.ResetColor();

            if (nl)
            {
                Console.Write(msg + "\n");
            }
            else
            {
                Console.Write(msg);
            }
        }
        public static void m(string msg, string friend, bool nl = true)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            ConsoleColor color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), "DarkYellow");
            Console.ForegroundColor = color;

            Console.Write("[" + DateTime.Now.ToString("T") + "] ");

            Console.ResetColor();

            Console.BackgroundColor = ConsoleColor.Black;
            ConsoleColor color2 = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), "DarkGray");
            Console.ForegroundColor = color2;

            Console.Write("[" + friend + "] ");

            Console.ResetColor();

            if (nl)
            {
                Console.Write(msg + "\n");
            }
            else
            {
                Console.Write(msg);
            }
        }
        public static void gm(string msg, string cname, string friend, bool nl = true)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            ConsoleColor color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), "DarkYellow");
            Console.ForegroundColor = color;

            Console.Write("[" + DateTime.Now.ToString("T") + "] ");

            Console.ResetColor();

            Console.BackgroundColor = ConsoleColor.Black;
            ConsoleColor color2 = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), "DarkGray");
            Console.ForegroundColor = color2;

            Console.Write("[" + cname + "] ");

            Console.ResetColor();

            Console.BackgroundColor = ConsoleColor.Black;
            ConsoleColor color3 = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), "DarkGray");
            Console.ForegroundColor = color3;

            Console.Write("[" + friend + "] ");

            Console.ResetColor();

            if (nl)
            {
                Console.Write(msg + "\n");
            }
            else
            {
                Console.Write(msg);
            }
        }
        public static void puts(string msg, string clr, bool nl = true, bool stamp = true)
        {
            if (stamp)
            {
                Console.BackgroundColor = ConsoleColor.Black;
                ConsoleColor color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), "DarkYellow");
                Console.ForegroundColor = color;

                Console.Write("[" + DateTime.Now.ToString("T") + "] ");

                Console.ResetColor();
            }

            if (nl)
            {
                Console.BackgroundColor = ConsoleColor.Black;
                ConsoleColor color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), clr);
                Console.ForegroundColor = color;

                Console.Write(msg + "\n");
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.Black;
                ConsoleColor color = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), clr);
                Console.ForegroundColor = color;

                Console.Write(msg, clr);
            }

            Console.ResetColor();
        }
    }
}

//       DarkBlue: This is foreground color DarkBlue.
//      DarkGreen: This is foreground color DarkGreen.
//       DarkCyan: This is foreground color DarkCyan.
//        DarkRed: This is foreground color DarkRed.
//    DarkMagenta: This is foreground color DarkMagenta.
//     DarkYellow: This is foreground color DarkYellow.
//           Gray: This is foreground color Gray.
//       DarkGray: This is foreground color DarkGray.
//           Blue: This is foreground color Blue.
//          Green: This is foreground color Green.
//           Cyan: This is foreground color Cyan.
//            Red: This is foreground color Red.
//        Magenta: This is foreground color Magenta.
//         Yellow: This is foreground color Yellow.
//          White: This is foreground color White.