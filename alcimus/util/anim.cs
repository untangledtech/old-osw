﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using Steam4NET;

namespace alcimus
{
    public class anim
    {
        static void alias(string gif)
        {
            alcimus.Program.context.SteamFriends.SetPersonaName("Olivia #" + gif);
        }
        static void alias_blnk(string gif)
        {
            alcimus.Program.context.SteamFriends.SetPersonaName("Olivia " + gif);
        }
        public static void create(string[] seq, int sleepytime = 500, bool blank = false)
        {
            log.p("alcimus creating animation with " + seq.Length + " frames");
            for (int i = 0; i < seq.Length; i++)
            {
                if (blank)
                {
                    alias_blnk(seq[i]);
                }
                else
                {
                    alias(seq[i]);
                }

                System.Threading.Thread.Sleep(sleepytime);
            }
        }
        static void alias2(string gif)
        {
            alcimus.Program.context.SteamFriends.SetPersonaName("Olivia " + gif);
        }
        public static void idle()
        {
            anim.create(new string[] { "|", "/", "—", "\\" }, 400, true);
            idle();
        }
        public static void blink()
        {
            anim.create(new string[] { "*", "", "*", "#" }, 250, true);
        }
        public static void init()
        {
            anim.create(new string[] {
                "i",
                "in",
                "ini",
                "init",
            });
            Thread.Sleep(2000);
            anim.create(new string[] {
                "ini-",
                "in-",
                "i-",
                "",
            });
        }
        public static void query()
        {
            anim.create(new string[] {
                "q",
                "qu",
                "que",
                "quer",
                "query",
            });
            Thread.Sleep(2000);
            anim.create(new string[] {
                "quer-",
                "que-",
                "qu-",
                "q-",
                "",
            });
        }
        public static void chat()
        {
            anim.create(new string[] {
                "m",
                "ms",
                "msg",
            });
            Thread.Sleep(1000);
            anim.create(new string[] {
                "ms-",
                "m-",
                "",
            });
        }
    }
}
