﻿using System;
using System.Threading;
using System.Text;
using System.Data;
using System.IO;
using System.Collections;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using Steam4NET;

namespace alcimus
{
    /*
    [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
    delegate Int32 NativeGetChatRoomEntry(IntPtr thisobj, UInt64 steamIDchat, Int32 iChatID, ref UInt64 steamIDuser, byte[] pvData, Int32 cubData, ref EChatEntryType peChatEntryType);

    [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
    delegate string NativeGetChatRoomName(IntPtr thisobj, UInt64 steamIDchat);
    */

    class Program
    {
        /*
        public static NativeGetChatRoomEntry get_cmsg;
        public static NativeGetChatRoomName get_cname;
        */
        public static string hour = DateTime.Now.ToString("HH");
        public static string h2 = DateTime.Now.ToString("dd");
        public static string day = DateTime.Now.ToString("MM/dd/yyyy ") + DateTime.Now.ToString("t");

        public static SteamContext context = new SteamContext();

        public static String user;
        public static Friend friend = null;

        public static int Clamp(int value, int min, int max)
        {
            if (value < min)
                return min;

            if (value > max)
                return max;

            return value;
        }
        static void ChatRoomEnter_OnRun(ChatRoomEnter_t param)
        {
            //anim.chat();
        }
        static void OpenChatDialog_OnRun(OpenChatDialog_t param)
        {
            //anim.chat();
        } 
        static void FriendChatMsg_OnRun(FriendChatMsg_t param)
        {
                

            byte[] message = new byte[1024 * 4];
            EChatEntryType type = EChatEntryType.k_EChatEntryTypeChatMsg;
            int len = context.SteamFriends.GetChatMessage(param.m_ulSender, (int)param.m_iChatID, message, message.Length, ref type);

            Friend friend = new Friend();

            friend.SteamID = param.m_ulSender;
            friend.PersonaName = context.SteamFriends.GetFriendPersonaName(friend.SteamID);
            if (context.SteamUser.GetSteamID() == param.m_ulSender) {
                log.p(Encoding.ASCII.GetString(message, 0, len));
                return;
            }
            string msg = Encoding.ASCII.GetString(message, 0, len - 1);
            if (type != EChatEntryType.k_EChatEntryTypeTyping && type != EChatEntryType.k_EChatEntryTypeLeftConversation)
            {
                string frnd = friend.PersonaName.ToLower().Trim();
                string logmsg = DateTime.Now.ToString("t") + " - " + friend.PersonaName + ": " + msg + "\r\n";



                log.m(msg, friend.PersonaName);

                string stamp = day + " ##" + h2 + friend.SteamID + hour + "##\r\n";
                string session = "##" + h2 + +friend.SteamID + hour + "##";
                if (File.Exists("logs/" + frnd + ".log"))
                {
                    string predat = File.ReadAllText("logs/" + frnd + ".log");
                    if (predat.Contains(session))
                    {
                        File.WriteAllText("logs/" + frnd + ".log", predat + logmsg);
                    }
                    else
                    {
                        File.WriteAllText("logs/" + frnd + ".log", stamp + predat + logmsg);
                    }
                }
                else
                {
                    File.WriteAllText("logs/" + frnd + ".log", stamp + logmsg);
                }
            }
            if (type == EChatEntryType.k_EChatEntryTypeLeftConversation)
            {
                string frnd = friend.PersonaName.ToLower().Trim();
                string logmsg = DateTime.Now.ToString("t") + " - " + friend.PersonaName + " has left the chat";



                log.p(friend.PersonaName + " has left the chat");

                string stamp = day + " ##" + h2 + +friend.SteamID + hour + "##\r\n";
                string session = "##" + h2 + +friend.SteamID + hour + "##";
                if (File.Exists("logs/" + frnd + ".log"))
                {
                    string predat = File.ReadAllText("logs/" + frnd + ".log");
                    if (predat.Contains(session))
                    {
                        File.WriteAllText("logs/" + frnd + ".log", predat + logmsg);
                    }
                    else
                    {
                        File.WriteAllText("logs/" + frnd + ".log", stamp + predat + logmsg);
                    }
                }
                else
                {
                    File.WriteAllText("logs/" + frnd + ".log", stamp + logmsg);
                }
            }
        }
        static void PersonaStateChange_OnRun( PersonaStateChange_t packet )
        {
            if (context.SteamFriends.GetFriendRelationship(packet.m_ulSteamID) == EFriendRelationship.k_EFriendRelationshipFriend)
            {
                string frnd = context.SteamFriends.GetFriendPersonaName(packet.m_ulSteamID);
                log.p(frnd + " has changed their state to " + context.SteamFriends.GetFriendPersonaState(packet.m_ulSteamID));
            }
        }
        /*
        static void ChatRoomMsg_OnRun(ChatRoomMsg_t msg)
        {
            byte[] mdata = new byte[1024 * 4];
            EChatEntryType type = EChatEntryType.k_EChatEntryTypeInvalid;
            ulong chatter = 0;

            string cname = get_cname(context.SteamFriends.Interface, msg.m_ulSteamIDChat);
            int len = get_cmsg(context.SteamFriends.Interface, msg.m_ulSteamIDChat, (int)msg.m_iChatID, ref chatter, mdata, mdata.Length, ref type);

            len = Clamp(len, 1, mdata.Length);

            log.gm(Encoding.UTF8.GetString(mdata, 0, len), get_cname(context.SteamFriends.Interface, msg.m_ulSteamIDChat), context.SteamFriends.GetFriendPersonaName(new CSteamID(msg.m_ulSteamIDUser)));
        }
        */
        static void Main(string[] args)
        {
            log.p("alcimus initializing", false);
            if (!context.Initialize())
                return;

            Program.user = context.SteamFriends.GetPersonaName();

            Console.WriteLine(" with user " + user + "...");

            log.p("alcimus creating hooks...");

            Console.WriteLine("\t\t\tFriendChatMsg_OnRun");
            context.FriendChatMsg.OnRun += new Callback<FriendChatMsg_t>.DispatchDelegate(FriendChatMsg_OnRun);

            Console.WriteLine("\t\t\tPersonaStateChange_OnRun");
            context.PersonaStateChange.OnRun += new Callback<PersonaStateChange_t>.DispatchDelegate(PersonaStateChange_OnRun);

            //Console.WriteLine("\t\t\tChatRoomMsg_OnRun");
            //context.ChatRoomMsg.OnRun += new Callback<ChatRoomMsg_t>.DispatchDelegate(ChatRoomMsg_OnRun);

            //Console.WriteLine("\t\t\tChatRoomEnter_OnRun");
            //context.ChatRoomEnter.OnRun += new Callback<ChatRoomEnter_t>.DispatchDelegate(ChatRoomEnter_OnRun);

            //Console.WriteLine("\t\t\tOpenChatDialog_OnRun");
            //context.OpenChatDialog.OnRun += new Callback<OpenChatDialog_t>.DispatchDelegate(OpenChatDialog_OnRun);

            log.p("alcimus injecting hooks...");
            context.StartCallbacks();

            log.p("alcimus creating buffer...");
            if (!Directory.Exists("logs"))
            {
                Directory.CreateDirectory("logs");
            }
            while (true)
            {
                Console.ReadLine();
                Thread.Sleep(0);
            }
        }
    }
}
