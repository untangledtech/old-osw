﻿using System;
using System.Threading;
using System.Text;
using System.Data;
using System.IO;
using System.Collections;
using System.Security.Cryptography;
using Steam4NET;

namespace chimera
{
    class Program
    {
        public static RSACryptoServiceProvider crypto;

        public static SteamContext context = new SteamContext();

        public static String user;
        public static Friend friend = null;
        public static String entangle = "_ERROR_";


        static void ChatRoomEnter_OnRun(ChatRoomEnter_t param)
        {
            //anim.chat();
        }
        static void OpenChatDialog_OnRun(OpenChatDialog_t param)
        {
            //anim.chat();
        } 
        static void FriendChatMsg_OnRun(FriendChatMsg_t param)
        {
            if (context.SteamUser.GetSteamID() == param.m_ulSender)
                return;

            byte[] message = new byte[1024 * 4];
            EChatEntryType type = EChatEntryType.k_EChatEntryTypeChatMsg;
            int len = context.SteamFriends.GetChatMessage(param.m_ulSender, (int)param.m_iChatID, message, message.Length, ref type);

            Friend friend = new Friend();

            friend.SteamID = param.m_ulSender;
            friend.PersonaName = context.SteamFriends.GetFriendPersonaName(friend.SteamID);

            string msg = Encoding.ASCII.GetString(message, 0, len - 1);
            if (entangle.Length > 0)
            {
                if (type != EChatEntryType.k_EChatEntryTypeTyping && friend.PersonaName.ToLower().Contains(entangle))
                {
                    friend.PersonaName = friend.PersonaName.Replace(" #", "");
                    //log.p(friend.PersonaName + " triggered hook FriendChatMsg_OnRun!");
                    if (msg.StartsWith("~%"))
                    {
                        log.p("received handshake from " + friend.PersonaName + " with size " + ( msg.Length - 2 ).ToString( ) + "!");
                        msg = msg.Substring(2);
                        File.WriteAllText("keys/" + friend.PersonaName.ToLower() + ".pub.xml", System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(msg)));

                        String key = "~@" + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(File.ReadAllText("keys/" + user + ".pub.xml")));
                        context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, System.Text.Encoding.UTF8.GetBytes(key), key.Length + 1);
                        log.p("bounced handshake with size " + key.Length + "!");
                    }
                    if (msg.StartsWith("~@"))
                    {
                        msg = msg.Substring(2);
                        File.WriteAllText("keys/" + friend.PersonaName.ToLower() + ".pub.xml", System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(msg)));
                    }
                    if (msg.StartsWith("~#"))
                    {
                        msg = msg.Substring(2);
                        crypto.FromXmlString(File.ReadAllText("keys/" + user + ".priv.xml"));
                        //log.p("loaded \"" + user + "\" rsa key");
                        byte[] decoded = crypto.Decrypt(Convert.FromBase64String(msg), false);

                        log.p(friend.PersonaName + ": " + System.Text.Encoding.UTF8.GetString(decoded));
                    }
                }
            }
        }
        static void Main(string[] args)
        {
            log.p("chimera initializing", false);
            //Console.Write("] chimera initializing");
            if (!context.Initialize())
                return;

            Program.user = context.SteamFriends.GetPersonaName().ToLower().Replace(" #", "");
            if (!Program.user.Contains("#"))
            {
                context.SteamFriends.SetPersonaName(Program.user + " #");
            }

            string user = context.SteamFriends.GetPersonaName().ToLower().Replace(" #", "");
            Console.WriteLine(" with user " + user + "...");

            log.p("chimera loading modules...");
            log.p("chimera using RSA cryptosystem");

            log.p("chimera creating hooks...");

            Console.WriteLine("\t\t\tFriendChatMsg_OnRun");
            context.FriendChatMsg.OnRun += new Callback<FriendChatMsg_t>.DispatchDelegate(FriendChatMsg_OnRun);

            //Console.WriteLine("\t\t\tChatRoomEnter_OnRun");
            //context.ChatRoomEnter.OnRun += new Callback<ChatRoomEnter_t>.DispatchDelegate(ChatRoomEnter_OnRun);

            //Console.WriteLine("\t\t\tOpenChatDialog_OnRun");
            //context.OpenChatDialog.OnRun += new Callback<OpenChatDialog_t>.DispatchDelegate(OpenChatDialog_OnRun);

            log.p("chimera injecting hooks...");
            context.StartCallbacks();

            log.p("chimera creating buffer...");
            int count = context.SteamFriends.GetFriendCount(4);
            for (int i = 0; i < count; i++)
            {
                ulong frnd = context.SteamFriends.GetFriendByIndex(i, 4);
                string alias = context.SteamFriends.GetFriendPersonaName(frnd);
                if (alias.Contains(" #"))
                {
                    log.p("\t" + alias + " using chimera");
                }
            }

            Directory.CreateDirectory("keys");
            log.p("chimera initializing RSA cryptosystem");
            if (!File.Exists("keys/" + user + ".pub.xml") || !File.Exists("keys/" + user + ".priv.xml"))
                rsac.newkey();
            else
                crypto = new RSACryptoServiceProvider();

            log.p("entangle with: ", false);
            entangle = Console.ReadLine();

            for (int i = 0; i < context.SteamFriends.GetFriendCount((int)EFriendFlags.k_EFriendFlagImmediate); ++i)
            {
                Friend _friend = new Friend();

                CSteamID steamId = context.SteamFriends.GetFriendByIndex(i, (int)EFriendFlags.k_EFriendFlagImmediate);
                string friendName = context.SteamFriends.GetFriendPersonaName(steamId);

                _friend.SteamID = steamId;
                _friend.PersonaName = friendName;

                if (_friend.PersonaName.ToLower().Contains(entangle))
                {
                    friend = _friend;
                    break;
                }
            }

            if (friend == null)
            {
                log.p("could not find person to entangle with!");
                Console.ReadKey();
                return;
            }
            log.p("ready: ", false);

            while (true)
            {
                String line = Console.ReadLine();

                if (line == "handshake")
                {
                    try
                    {
                        String key = "~%" + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(File.ReadAllText("keys/" + user + ".pub.xml")));
                        context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, System.Text.Encoding.UTF8.GetBytes(key), key.Length + 1);
                        log.p("sent handshake!");
                    }
                    catch(Exception e)
                    {
                        log.p("handshake with user " + entangle + " failed!");
                        log.puts(e.ToString(), "DarkRed");
                    }
                }
                else if (line == "entangle")
                {
                    log.p("entangle with: ", false);
                    entangle = Console.ReadLine();
                    bool found = false;

                    for (int i = 0; i < context.SteamFriends.GetFriendCount((int)EFriendFlags.k_EFriendFlagImmediate); ++i)
                    {
                        Friend _friend = new Friend();

                        CSteamID steamId = context.SteamFriends.GetFriendByIndex(i, (int)EFriendFlags.k_EFriendFlagImmediate);
                        string friendName = context.SteamFriends.GetFriendPersonaName(steamId);

                        _friend.SteamID = steamId;
                        _friend.PersonaName = friendName;

                        if (_friend.PersonaName.ToLower().Contains(entangle))
                        {
                            friend = _friend;
                            found = true;
                            break;
                        }
                    }
                    log.p("ready: ", false);

                    if (!found)
                    {
                        log.p("could not find person to entangle with!");
                    }
                }
                else if (line == "auth")
                {
                    log.p("loading authtable... ", false);
                    Directory.CreateDirectory("auth");

                    Console.WriteLine("done!");


                    log.p("loading keytable... ", false);
                    string[] keylist = Directory.GetFiles("keys/");
                    int keys = keylist.Length;
                    Console.WriteLine("done!");
                    if (keys > 0)
                    {
                        foreach (string _key in keylist)
                        {
                            if (!_key.Contains(user))
                            {
                                log.p("\t" + _key);
                            }
                        }
                    }
                    log.p("key you want to authorize: ", false);

                    string input3 = Console.ReadLine();
                    try
                    {
                        log.p("copying your key... ", false);
                        File.Copy(input3, "auth/" + input3.Replace("keys/", ""));
                        Console.WriteLine("done!");
                    }
                    catch
                    {
                        log.p("invalid key specified!", false);
                    }
                }
                else if (line == "authtable")
                {
                    log.p("loading authtable... ", false);
                    Directory.CreateDirectory("auth");

                    string[] authed_users = Directory.GetFiles("auth/");
                    int uc = authed_users.Length;
                    if (uc > 0)
                    {
                        Console.WriteLine("done!");
                        foreach (string _user in authed_users)
                        {
                            log.p(_user);
                        }
                    }
                    else
                    {
                        Console.WriteLine("done!");
                        log.p("no authenticated users found");
                        log.p("would you like to authenticate one of your keys? (y/n): ", false);

                        string input2 = Console.ReadLine();
                        if (input2 == "y" || input2 == "yes")
                        {
                            string[] keylist = Directory.GetFiles("keys/");
                            int keys = keylist.Length;
                            if (keys > 0)
                            {
                                foreach (string _key in keylist)
                                {
                                    if (!_key.Contains(user))
                                    {
                                        log.p("\t" + _key);
                                    }
                                }
                            }
                            log.p("key you want to authorize: ", false);

                            string input3 = Console.ReadLine();
                            try
                            {
                                log.p("copying your key... ", false);
                                File.Copy(input3, "auth/" + input3.Replace("keys/", ""));
                                Console.WriteLine("done!");
                            }
                            catch
                            {
                                log.p("invalid key specified!", false);
                            }
                        }
                    }
                }
                else if (line == "testanim idle")
                {
                    anim.idle();
                }
                else if (line == "testanim init")
                {
                    anim.init();
                }
                else
                {
                    string fixed_name = friend.PersonaName.ToLower().Replace(" #", "");
                    if (!File.Exists("keys/" + fixed_name + ".pub.xml"))
                    {
                        log.p("missing RSA key for " + friend.PersonaName + ", please handshake!");
                        continue;
                    }
                    //log.p(File.ReadAllText("keys/" + fixed_name + ".pub.xml").Length.ToString());
                    RSACryptoServiceProvider _crypto = new RSACryptoServiceProvider();

                    
                    try
                    {
                        _crypto.FromXmlString(File.ReadAllText("keys/" + fixed_name + ".pub.xml"));
                    }
                    catch(Exception e)
                    {
                        log.p("it looks like the cryptosystem you're referencing has broke :<");
                        log.p("keys/" + fixed_name + ".pub.xml");
                        log.p(File.ReadAllText("keys/" + fixed_name + ".pub.xml"));
                        log.puts(e.ToString(), "DarkRed");
                    }
                    String data = Convert.ToBase64String(_crypto.Encrypt(System.Text.Encoding.UTF8.GetBytes(line), false));
                    String text = "~#" + data;
                    context.SteamFriends.SendMsgToFriend(friend.SteamID, EChatEntryType.k_EChatEntryTypeChatMsg, System.Text.Encoding.UTF8.GetBytes(text), text.Length + 1);
                }
            }
        }
    }
}
